package main

import (
  "os"
  "fmt"
  "time"
  "strconv"
)

const poison_pill = -1

func pinger(n int64, in chan int64, out chan int64, done chan bool) {
  for {
    value := <- in
    if (value != poison_pill){
      next := value + 1
      // fmt.Printf("[PINGER] ping! %d \n",next)    
      if next > n {
        out <- poison_pill
        break
      } else {
        out <- next
      }
    } else {
      break
    }
  }
  fmt.Println("[PINGER] end.\n")   
  done <- true
}

func ponger(n int64, in chan int64, out chan int64, done chan bool) {
  for {
    value := <- in
    if (value != poison_pill){
      next := value + 1
      // fmt.Printf("[PONGER] pong! %d \n",next)    
      if next > n {
        out <- poison_pill
        break
      } else {
        out <- next
      }
    } else {
      break
    }
  }
  fmt.Println("[PONGER] end.\n")    
  done <- true
}


func main() {

  n,_ := strconv.ParseInt(os.Args[1], 10, 64)

  var pingch chan int64 = make(chan int64)
  var pongch chan int64 = make(chan int64)
  var done chan bool = make(chan bool)

  // var n int64 = 1000

  t0 := time.Now()

  go pinger(n, pongch, pingch, done)
  go ponger(n, pingch, pongch, done)

  pongch <- 1
  
  <- done
  <- done

  t1 := time.Now()

  fmt.Printf("Done %d in %v \n", n, t1.Sub(t0))
}
