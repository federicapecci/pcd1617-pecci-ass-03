package pcd.ass02.ex1.sol;

import pcd.ass02.ex1.*;

public class GuessTheNumber {

	public static void main(String[] args) {
		int nPlayers = Integer.parseInt(args[0]);
		OracleInterface oracle = new OracleImpl(nPlayers);
		for (int i = 0; i < nPlayers; i++){
			new Player(i,oracle).start();
		}		
	}

}
