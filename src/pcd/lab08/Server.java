package pcd.lab08;
import io.vertx.core.AbstractVerticle;

public class Server extends AbstractVerticle {

  int i = 0;  
	
  public void start() {

	vertx.createHttpServer().requestHandler(req -> {
        
      System.out.println("request "+i+" arrived.");
          
      String fileName = req.path().substring(1);
      
      System.out.println(fileName);
      
      vertx.fileSystem().readFile(fileName, result -> {
    	  
            System.out.println("result ready");

    	    if (result.succeeded()) {
    	        System.out.println(result.result());

    	        req.response()
    	        	.putHeader("content-type", "text/plain")
    	        	.end(result.result().toString());
    	        
    	    } else {
    	        System.err.println("Oh oh ..." + result.cause());

    	        req.response()
	        	.putHeader("content-type", "text/plain")
	        	.end("File not found");
    	    
    	    }
      });
      
      
      i++;
      
    }).listen(8081);
  }
}