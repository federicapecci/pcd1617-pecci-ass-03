package pcd.ass01.ex2.sol;

import java.util.concurrent.Semaphore;

public class Pinger extends Player {
	public Pinger(Counter counter, Semaphore iCanGo, Semaphore friendCanGo, StopFlag stop, Output output, Semaphore shutdown){
		super("ping!",counter,iCanGo,friendCanGo,stop,output, shutdown);
	}	
}
