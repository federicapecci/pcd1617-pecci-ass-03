package pcd.ass01.ex2.sol;

import java.util.LinkedList;
import java.util.List;

public class Counter {

	private List<CounterObserver> observers;

	private int cont;
	
	public Counter(int base){
		this.cont = base;
		observers = new LinkedList<CounterObserver>();
	}
	
	public void inc(){
		cont++;

		for (CounterObserver obs: observers){
			obs.counterUpdated(cont);
		}
		
	}
	
	public void addObserver(CounterObserver obs){
		observers.add(obs);
	}
	
	public void close(){
		for (CounterObserver obs: observers){
			obs.counterClosed();
		}
	}
	
	public int getValue(){
		return cont;
	}
	
}
